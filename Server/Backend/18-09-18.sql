-- mysqldump-php https://github.com/ifsnop/mysqldump-php
--
-- Host: localhost	Database: AnastasiaDB
-- ------------------------------------------------------
-- Server version 	5.7.23
-- Date: Tue, 18 Sep 2018 21:59:22 +0000

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `anastasiadb_eventos`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anastasiadb_eventos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(40) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT '',
  `fecha_activacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `estado` enum('creado','retrasado','terminado','diario','semanal','mensual','anual') DEFAULT NULL,
  `tipo_alerta` enum('vibrar','timbrar') DEFAULT NULL,
  `referencia` varchar(255) DEFAULT NULL,
  `mensaje` varchar(255) DEFAULT NULL,
  `idEventoPrevio` int(11) DEFAULT '-1',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activo` enum('S','N') DEFAULT 'S',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anastasiadb_eventos`
--

LOCK TABLES `anastasiadb_eventos` WRITE;
/*!40000 ALTER TABLE `anastasiadb_eventos` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `anastasiadb_eventos` VALUES (1,'lY56XGn5VNaX40P4AWUWlNcghMsgAQBVVLLmju6e','dia de independencia','2018-09-18 21:40:35','anual','','','',NULL,'2018-09-18 21:40:35','S');
/*!40000 ALTER TABLE `anastasiadb_eventos` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `anastasiadb_log`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anastasiadb_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(40) DEFAULT '',
  `idSesiones` int(11) DEFAULT NULL,
  `tipo` enum('A','E','M') DEFAULT NULL,
  `consulta` text,
  `activo` enum('S','N') NOT NULL DEFAULT 'S',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anastasiadb_log`
--

LOCK TABLES `anastasiadb_log` WRITE;
/*!40000 ALTER TABLE `anastasiadb_log` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `anastasiadb_log` VALUES (1,'MYnsXFnBw6FC4XxaAxKWtNRgi5q6chjy0cL80uee',1,'A','INSERT INTO anastasiadb_sesiones \n                            (uid, ip, host, \n                            ultimo_acceso)\n                            VALUES\n                            (\'IYWMXjnjH5Fa4JoDABBWRNqgr5qicFjyTHLHauPe\', \'127.0.0.1\', \'localhost\', \n                            \'18-09-18 21:38:44\')','S','2018-09-18 21:38:44'),(2,'7YHpXeny0KiN4tppAEpWiNtgy5qmeX24f4La3uge',2,'A','INSERT INTO anastasiadb_sesiones \n                            (uid, ip, host, \n                            ultimo_acceso)\n                            VALUES\n                            (\'xYL3XQnywuDh4iYCAHhWzNQgF5qReF24rRLc3uOe\', \'127.0.0.1\', \'localhost\', \n                            \'18-09-18 21:38:51\')','S','2018-09-18 21:38:51'),(3,'7Y5CX9nyouD84pTcAqXWBNFgM5K2m7VyyGLTPuXe',3,'A','INSERT INTO anastasiadb_sesiones \n                            (uid, ip, host, \n                            ultimo_acceso)\n                            VALUES\n                            (\'BYouX0n2Ys5d4KImANoWyNQgl5KxmwVy3WLmJu1e\', \'127.0.0.1\', \'localhost\', \n                            \'18-09-18 21:39:06\')','S','2018-09-18 21:39:06'),(4,'lYluXYn2Y1Oz4SgEAZOWTNLg45KRcT2ydKLJXu6e',4,'A','INSERT INTO anastasiadb_sesiones \n                            (uid, ip, host, \n                            ultimo_acceso)\n                            VALUES\n                            (\'2YTgX2n2wfQQ4iorAzhWENQg25Kicm2yEaLzrude\', \'127.0.0.1\', \'localhost\', \n                            \'18-09-18 21:39:41\')','S','2018-09-18 21:39:41'),(5,'uYimX1n7tIi04TCCA0mWfNYglMsXk0UR2cLztuhe',5,'A','INSERT INTO anastasiadb_sesiones \n                            (uid, ip, host, \n                            ultimo_acceso)\n                            VALUES\n                            (\'YYupXSnynuad4ZJVAhwWhNyg2MsUkoUR6CLjJu9e\', \'127.0.0.1\', \'localhost\', \n                            \'18-09-18 21:40:10\')','S','2018-09-18 21:40:10'),(6,'GYFHXDnjwgaP4B5fALdW4NmgAMsBAIBVoiLuCuPe',6,'A','INSERT INTO anastasiadb_sesiones \n                            (uid, ip, host, \n                            ultimo_acceso)\n                            VALUES\n                            (\'IYdgXYn709Dw4fstA2nWSNsgcMs7ADBVnHLQku3e\', \'127.0.0.1\', \'localhost\', \n                            \'18-09-18 21:40:35\')','S','2018-09-18 21:40:35'),(7,'ZYitXvn5nqQB4hfnAc3WYNrguMsVAcBVQlLsrude',6,'A','INSERT INTO anastasiadb_eventos \n                        (uid, descripcion, \n                        fecha_activacion, estado, tipo_alerta, \n                        referencia, mensaje, idEventoPrevio)\n                        VALUES\n                        (\'lY56XGn5VNaX40P4AWUWlNcghMsgAQBVVLLmju6e\', \'dia de independencia\', \n                        \'2018-09-18 14:40:35\', \'anual\', \'\', \n                        \'\', \'\', NULL)','S','2018-09-18 21:40:35'),(8,'ZYb2XRn2YLQk4T6iAMbWgNmgXMxImmUVujLcbuTe',7,'A','INSERT INTO anastasiadb_sesiones \n                            (uid, ip, host, \n                            ultimo_acceso)\n                            VALUES\n                            (\'KYfiX1n2MUO44uVWAUhWbNegqMxnmkUVChL3Wuoe\', \'127.0.0.1\', \'localhost\', \n                            \'18-09-18 21:41:00\')','S','2018-09-18 21:41:00'),(9,'OYbeX0nVHbWB4E8EA7YWsNogJMbZekB4fcLIYuSe',8,'A','INSERT INTO anastasiadb_sesiones \n                            (uid, ip, host, \n                            ultimo_acceso)\n                            VALUES\n                            (\'eYdIXVnVVHaU4Z0FAhaWBNvglMbRe6B4FXLpjufe\', \'127.0.0.1\', \'localhost\', \n                            \'18-09-18 21:45:55\')','S','2018-09-18 21:45:55'),(10,'lYaLXpnPMhWH4f1VApHWZN5g1MoAmy2q8QL1YuOe',9,'A','INSERT INTO anastasiadb_sesiones \n                            (uid, ip, host, \n                            ultimo_acceso)\n                            VALUES\n                            (\'TYZ0Xvnc0BDV4z5DAmqWfNPghModmp2qDoLKRu0e\', \'127.0.0.1\', \'localhost\', \n                            \'18-09-18 21:46:01\')','S','2018-09-18 21:46:01'),(11,'JYk9XLnytHgO4RmcAM3WnNxg1MoTkiPLRYLcMuLe',10,'A','INSERT INTO anastasiadb_sesiones \n                            (uid, ip, host, \n                            ultimo_acceso)\n                            VALUES\n                            (\'aY5QXzn2HX5K4obcAeCWVNvgeMoTkbPLlYLEiuJe\', \'127.0.0.1\', \'localhost\', \n                            \'18-09-18 21:46:18\')','S','2018-09-18 21:46:18'),(12,'tYEMX0nPogWi4brrA6NWANugtMoh8t2q7qLlFuie',11,'A','INSERT INTO anastasiadb_sesiones \n                            (uid, ip, host, \n                            ultimo_acceso)\n                            VALUES\n                            (\'nYxwXHncYSoF4MjPAlbW5N0gWMoZ8a2qAQLQcute\', \'127.0.0.1\', \'localhost\', \n                            \'18-09-18 21:46:21\')','S','2018-09-18 21:46:21');
/*!40000 ALTER TABLE `anastasiadb_log` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `anastasiadb_sesiones`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anastasiadb_sesiones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(40) DEFAULT NULL,
  `ip` varchar(15) NOT NULL DEFAULT '''''',
  `host` varchar(50) NOT NULL DEFAULT '''''',
  `ultimo_acceso` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `activo` enum('S','N') DEFAULT 'S',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anastasiadb_sesiones`
--

LOCK TABLES `anastasiadb_sesiones` WRITE;
/*!40000 ALTER TABLE `anastasiadb_sesiones` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `anastasiadb_sesiones` VALUES (1,'IYWMXjnjH5Fa4JoDABBWRNqgr5qicFjyTHLHauPe','127.0.0.1','localhost','2018-09-19 04:38:44','S','2018-09-18 21:38:44'),(2,'xYL3XQnywuDh4iYCAHhWzNQgF5qReF24rRLc3uOe','127.0.0.1','localhost','2018-09-19 04:38:51','S','2018-09-18 21:38:51'),(3,'BYouX0n2Ys5d4KImANoWyNQgl5KxmwVy3WLmJu1e','127.0.0.1','localhost','2018-09-19 04:39:06','S','2018-09-18 21:39:06'),(4,'2YTgX2n2wfQQ4iorAzhWENQg25Kicm2yEaLzrude','127.0.0.1','localhost','2018-09-19 04:39:41','S','2018-09-18 21:39:41'),(5,'YYupXSnynuad4ZJVAhwWhNyg2MsUkoUR6CLjJu9e','127.0.0.1','localhost','2018-09-19 04:40:10','S','2018-09-18 21:40:10'),(6,'IYdgXYn709Dw4fstA2nWSNsgcMs7ADBVnHLQku3e','127.0.0.1','localhost','2018-09-19 04:40:35','S','2018-09-18 21:40:35'),(7,'KYfiX1n2MUO44uVWAUhWbNegqMxnmkUVChL3Wuoe','127.0.0.1','localhost','2018-09-19 04:41:00','S','2018-09-18 21:41:00'),(8,'eYdIXVnVVHaU4Z0FAhaWBNvglMbRe6B4FXLpjufe','127.0.0.1','localhost','2018-09-19 04:45:55','S','2018-09-18 21:45:55'),(9,'TYZ0Xvnc0BDV4z5DAmqWfNPghModmp2qDoLKRu0e','127.0.0.1','localhost','2018-09-19 04:46:01','S','2018-09-18 21:46:01'),(10,'aY5QXzn2HX5K4obcAeCWVNvgeMoTkbPLlYLEiuJe','127.0.0.1','localhost','2018-09-19 04:46:18','S','2018-09-18 21:46:18'),(11,'nYxwXHncYSoF4MjPAlbW5N0gWMoZ8a2qAQLQcute','127.0.0.1','localhost','2018-09-19 04:46:21','S','2018-09-18 21:46:21');
/*!40000 ALTER TABLE `anastasiadb_sesiones` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on: Tue, 18 Sep 2018 21:59:22 +0000
